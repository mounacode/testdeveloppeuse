<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fiche Contact</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
     <!-- post contact -->
    <?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    include 'index.php';
}
if (isset($_POST['button'])) {
    extract($_POST);
    if (isset($nom) && isset($prenom) && isset($categorie)) {
        include_once 'connexion_dbb.php';
        $req = mysqli_query($con, "INSERT INTO contacts VALUES(NULL, '$nom','$prenom', '$categorie') ");
        if ($req) {
            header('Location:index.php');
        } else {
            $message = 'contact non ajouter';
        }
    } else {
        $message = 'veuillez remplir les champs';
        echo $message;
    }
}

?>
<!-- popup ajouter -->
        <a onclick="toggle()" class="btn_add">ajouter</a>
        <div id="popup" class="popup">
           <a onclick="toggle()"class="back-btn">Annuler</a>
           <h2>Ajouter un contact</h2>
           <p class="erreur">
           <?php
if (isset($message)) {
    echo $message;
}

?>
           </p>
           <form action="" method="POST">
               <label for="nom">Nom :</label>
               <input type="text" id="nom" name="nom">
               <label for="prenom">Prenom:</label>
               <input type="text" id="prenom" name="prenom">
               <label for="categorie">Categorie:</label>
               <input type="text" id="categorie" name="categorie">
               <input type="submit" value="Envoyer" name="button">

            </form>
        </div>

        <!-- Liste des contact -->
        <h2>Listes des contacts</h2>
        <!-- inclure la base de donne -->

        <input type="text" id="search" placeholder="Search...">
        <table>
            <tr class="items">
                <th>Nom</th>
                <th>Prenom</th>
                <th>Categorie</th>
                <th>Modifier</th>
                <th>Suprimer</th>
            </tr>
            <?php
include_once 'connexion_dbb.php';
//    afficher la liste des conctact
$req = mysqli_query($con, "SELECT * FROM contacts ");
if (mysqli_num_rows($req) == 0) {
    echo 'il n\'y pas plus de contacts';
} else {
    while ($row = mysqli_fetch_assoc($req)) {
        ?>

          <tr onclick="window.location.href='update.php?id=<?=$row['id']?>'">
                <td><?=$row['nom']?></td>
                <td><?=$row['prenom']?></td>
                <td><?=$row['categorie']?></td>
                <td><a href="modifier.php?id=<?=$row['id']?>" ><img src="images/edit.svg" alt=""></a></td>
                <td><a href="supprimer.php?id=<?=$row['id']?>"> <img src="images/trash-2.svg" alt=""></a></td>
            </tr>


       <?php
}
}

?>

        </table>



    </div>
    <script>
           function toggle() {
            var popup = document.getElementById('popup');
            if (popup.style.display === 'none') {
                popup.style.display = 'block';
            } else {
                popup.style.display = 'none';
            }
        }

       $(document).ready(function(){
            $('#search').keyup(function(){
                var query = $(this).val();
                if(query != ''){
                    $.ajax({
                        url:"index.php",
                        method:"POST",
                        data:{query:query},
                        success:function(data){
                            $('#popup').html(data);
                        }
                    });
                }
            });
        });
    </script>
</body>
</html>
